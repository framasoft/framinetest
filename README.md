[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l'inscription n'est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

# Framinetest

Framinetest est la page d'accueil du jeu en ligne Minetest que l'association Framasoft propose sur le site : https://framinetest.org
La personnalisation s'appuie sur la librairie Bootstrap dont les fichiers se trouvent dans la [Framanav](https://framagit.org/framasoft/framanav).

## Vacances

Pour ajouter le message d’avertissement de coupure du serveur pendant les vacances, modifier `src/data/main.yml` et passer `meta.holidays` à `True`.
